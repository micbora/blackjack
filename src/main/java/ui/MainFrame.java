package ui;

import bm.Card;
import bm.DefaultDeckCards;
import bm.Game;
import bm.RandomDeckCards;
import bm.Strategy;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.NumberFormat;

/**
 * Główne okno programu.
 */
public class MainFrame extends JFrame {

    private TextArea logs;
    private JPanel computerHandPanel;
    private JPanel playerHandPanel;
    private Game game;
    private int seed;
    private int iterNr;
    private MainFrame frameObj;

    public MainFrame() throws HeadlessException {
        super("BlackJack");
        seed = -1;
        iterNr = 1;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setLayout(new GridLayout(1, 2, 10, 10));
        //add(createGamePanel(), BorderLayout.NORTH);
        add(createSettingsPanel(), BorderLayout.WEST);
        add(createLogPanel(), BorderLayout.EAST);
        pack();
        frameObj = this;
    }

    private JPanel createGamePanel() {
        JPanel gamePanel = new JPanel(new GridLayout(1, 2));
        computerHandPanel = new JPanel(new FlowLayout());
        playerHandPanel = new JPanel(new FlowLayout());
        gamePanel.add(computerHandPanel);
        gamePanel.add(playerHandPanel);
        return gamePanel;
    }

    private JPanel createSettingsPanel() {
        JPanel settingsPanel = new JPanel(new GridLayout(8, 1, 5, 5));
        settingsPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 5));

        //Rodzaj talii
        JRadioButton traditionalDeckRadioButton = new JRadioButton("Klasyczna talia");
        JRadioButton randomDeckRadioButton = new JRadioButton("Losowa talia");
        randomDeckRadioButton.setSelected(true);

        ButtonGroup deckRadioButton = new ButtonGroup();
        deckRadioButton.add(traditionalDeckRadioButton);
        deckRadioButton.add(randomDeckRadioButton);

        settingsPanel.add(traditionalDeckRadioButton);
        settingsPanel.add(randomDeckRadioButton);

        //Dobieranie kart przez kasyno
        JCheckBox checkBox = new JCheckBox("Czy kasyno może dobierać karty?");
        settingsPanel.add(checkBox);

        //Polityki
        JComboBox<Strategy> strategies = new JComboBox<>(Strategy.values());
        settingsPanel.add(strategies);


        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMinimumFractionDigits(0);

        //Ziarno
        JFormattedTextField seedNumber = new JFormattedTextField(numberFormat);
        seedNumber.setColumns(10);
        seedNumber.setText("Ziarno");
        seedNumber.addPropertyChangeListener("value", evt -> {
            if (evt.getNewValue() instanceof Number) {
                seed = ((Long) evt.getNewValue()).intValue();
            } else if (evt.getNewValue() instanceof String) {
                String val = (String) evt.getNewValue();
                try {
                    seed = Integer.parseInt(val);
                } catch (NumberFormatException e) {
                    seed = -1;
                }
            }
        });
        settingsPanel.add(seedNumber);

        //Iteracje
        JFormattedTextField iterationNumber = new JFormattedTextField(numberFormat);
        iterationNumber.setColumns(10);
        iterationNumber.setText("Liczba iteracji");
        iterationNumber.addPropertyChangeListener("value", evt -> {
            if (evt.getNewValue() instanceof Number) {
                iterNr = ((Long) evt.getNewValue()).intValue();
            } else if (evt.getNewValue() instanceof String) {
                String val = (String) evt.getNewValue();
                try {
                    iterNr = Integer.parseInt(val);
                } catch (NumberFormatException e) {
                    iterNr = 1;
                }
            }
        });
        settingsPanel.add(iterationNumber);

        //Buttony
        Button startButton = new Button("Rozegraj grę");
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                game = new Game(traditionalDeckRadioButton.isSelected() ? new DefaultDeckCards(seed) : new RandomDeckCards(seed), frameObj, checkBox.isSelected());
                game.setPlayerStaretegy((Strategy)strategies.getSelectedItem());
                for (int i = 1; i<= iterNr; ++i) {
                    boolean res = game.firstDeal(i);
                    if (!res) {
                        do {
                            res = game.gameIteration();
                        }
                        while(res);
                    }
                }
                game.printResults(iterNr);
            }
        });
        Button clearButton = new Button("Wyczyść grę");
        clearButton.addActionListener(e -> {
            game = null;
            //clearPanels();
        });

        settingsPanel.add(startButton);
        settingsPanel.add(clearButton);

        return settingsPanel;
    }

    private void clearPanels() {
        computerHandPanel.removeAll();
        playerHandPanel.removeAll();
        revalidate();
    }

    private JPanel createLogPanel() {
        JPanel logPanel = new JPanel(new GridLayout(1,1));
        logs = new TextArea("");
        logs.enableInputMethods(false);
        logPanel.add(logs);
        return logPanel;
    }

    public void addLogLine(String s) {
        logs.append("\n");
        logs.append(s);
    }

    public void addCard(Card c, boolean toPlayer) {
        JLabel cardLabel = new JLabel(c.toString());
        cardLabel.setHorizontalTextPosition(SwingConstants.CENTER);
        cardLabel.setVerticalAlignment(SwingConstants.CENTER);
        JPanel card = new JPanel(new GridLayout(1, 1));
        card.add(cardLabel);
        card.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        if (toPlayer) {
            playerHandPanel.add(card);
            revalidate();
        } else {
            computerHandPanel.add(card);
            revalidate();
        }
    }
}
