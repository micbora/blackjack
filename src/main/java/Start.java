import ui.MainFrame;

import java.awt.EventQueue;

/**
 * Klasa startowa aplikacji.
 */
public class Start {

    public static void main(String[] args) {
        EventQueue.invokeLater(MainFrame::new);
    }
}
