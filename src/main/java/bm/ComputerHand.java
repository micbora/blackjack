package bm;

/**
 * Klasa ręki komputera.
 */
public class ComputerHand extends Hand {

    private boolean newCards;
    /**
     * Konstruktor.
     */
    public ComputerHand(boolean newCards) {
        super("Komputer");
        setStrategy(Strategy.COMPUTER);
        this.newCards = newCards;
    }

    public boolean isNewCards() {
        return newCards;
    }

    public void addNewCard(Card c, boolean isVisible, boolean initial) {
        if (!newCards && !initial) return;
        getVisibleCards().addAll(getInvisibleCards());
        getInvisibleCards().clear();
        super.addNewCard(c, isVisible);
    }

    @Override
    public void addNewCard(Card c, boolean isVisible) {
        getVisibleCards().addAll(getInvisibleCards());
        getInvisibleCards().clear();
        super.addNewCard(c, isVisible);
    }
}
