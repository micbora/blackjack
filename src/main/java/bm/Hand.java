package bm;

import java.util.LinkedList;
import java.util.List;

/**
 * Dłoń gracza.
 */
public class Hand {

    private String name;
    private List<Card> visibleCards;
    private List<Card> invisibleCards;

    private Strategy strategy;

    private int points;
    private int[] wins;
    private int[] loses;

    /**
     * Konstruktor.
     */
    public Hand(String name) {
        visibleCards = new LinkedList<>();
        invisibleCards = new LinkedList<>();
        strategy = null;
        this.name = name;
        points = 0;
        wins = new int[22];
        loses = new int[22];
    }

    /**
     * Lista widocznych kart na ręce.
     *
     * @return lista widocznych kart na ręce
     */
    public List<Card> getVisibleCards() {
        return visibleCards;
    }

    /**
     * Lista niewidocznych kart na ręce.
     *
     * @return lista niewidocznych kart na ręce
     */
    public List<Card> getInvisibleCards() {
        return invisibleCards;
    }

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public String getName() {
        return name;
    }

    /**
     * Dodanie nowej karty ze stosu na rękę.
     *
     * @param c         karta
     * @param isVisible czy jest widoczna
     */
    public void addNewCard(Card c, boolean isVisible) {
        if (isVisible) {
            visibleCards.add(c);
        } else {
            invisibleCards.add(c);
        }
    }

    /**
     * Oblicza wartość kart na ręce.
     *
     * @return suma wartości kart
     */
    public int sumHandValue(boolean visible) {
        int aces = 0;
        int value = 0;
        for (Card c : visibleCards) {
            if (c.getValue().equals(CardValue.vA)) {
                ++aces;
            }
            value += c.getCardValue();
        }
        if (!visible) {
            for (Card c : invisibleCards) {
                if (c.getValue().equals(CardValue.vA)) {
                    ++aces;
                }
                value += c.getCardValue();
            }
        }
        if (value > 21) {
            for (int i = 0; i < aces; ++i) {
                value = value - 10;
                if (value <= 21) {
                    break;
                }
            }
        }
        return value;
    }

    public void refreshHand() {
        visibleCards = new LinkedList<>();
        invisibleCards = new LinkedList<>();
    }

    public void addPoints(int value) {
        points += value;
    }

    public void addWin(int points) {
        wins[points]++;
    }

    public void addLose(int points) {
        loses[points]++;
    }

    public int getPoints() {
        return points;
    }

    public int[] getWins() {
        return wins;
    }

    public int[] getLoses() {
        return loses;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Card c: getVisibleCards()) {
            sb.append(c).append(' ');
        }
        for (Card c: getInvisibleCards()) {
            sb.append("XX").append(' ');
        }
        return sb.toString();
    }

    public String toCompleteString() {
        StringBuilder sb = new StringBuilder();
        for (Card c: getVisibleCards()) {
            sb.append(c).append(' ');
        }
        for (Card c: getInvisibleCards()) {
            sb.append(c).append(' ');
        }
        return sb.toString();
    }
}
