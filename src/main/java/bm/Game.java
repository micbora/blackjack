package bm;

import ui.MainFrame;

/**
 * Serwis przebiegu gry.
 */
public class Game {

    private Hand player;
    private ComputerHand computer;
    private DeckCards deckCards;
    private Hand currentHand;
    private MainFrame frame;

    /**
     * Konstruktor.
     *
     * @param deckCards talia kart
     */
    public Game(DeckCards deckCards, MainFrame mainFrame, boolean computerNewCards) {
        computer = new ComputerHand(computerNewCards);
        player = new Hand("Gracz");
        this.deckCards = deckCards;
        frame = mainFrame;
        printGameStart();
    }

    /**
     * Rozdanie kart na początku gry.
     */
    public boolean firstDeal(int iteration) {
        player.refreshHand();
        computer.refreshHand();
        printPlayStart(iteration);
        computer.addNewCard(deckCards.getNextCard(), true, true);
        computer.addNewCard(deckCards.getNextCard(), false, true);
        player.addNewCard(deckCards.getNextCard(), true);
        player.addNewCard(deckCards.getNextCard(), true);
        frame.addLogLine("Stan po rozdaniu:");
        printPlayerState();
        printComputerState();

        if (player.sumHandValue(false) == 21) {
            printPlayerWin();
            player.addPoints(Points.WIN.getValue());
            player.addWin(0);
            return true;
        }
        if (computer.sumHandValue(false) == 21) {
            printComputerWin();
            player.addPoints(Points.LOSE.getValue());
            player.addLose(0);
            return true;
        }
        return false;
    }

    /**
     * Iteracja gry - decyzja o następnym ruchu zgodnie z polityką, reakcja na rezultat.
     *
     * @return rezultat akcji
     */
    public boolean gameIteration() {
        currentHand = computer;
        Action nextComputerMove = computer.isNewCards() ? computer.getStrategy().getFunction().apply(this) : new Action("Komputer nie wykonuje ruchu", ActionType.PASS);
        printComputerAction(nextComputerMove.getRes());
        currentHand = player;
        Action nextPlayerMove = player.getStrategy().getFunction().apply(this);
        printPlayerAction(nextPlayerMove.getRes());
        if(nextComputerMove.getType().isStopStatus() && nextPlayerMove.getType().isStopStatus()) {
            finalizePlay();
            return false;
        }
        if (!nextComputerMove.getType().isStopStatus()) {
            computer.addNewCard(deckCards.getNextCard(), false, false);
        }
        if (!nextPlayerMove.getType().isStopStatus()) {
            player.addNewCard(deckCards.getNextCard(), true);
        }
        printPlayerState();
        printComputerState();
        if (player.sumHandValue(false) >= 21) {
            finalizePlay();
            return false;
        }
        return true;
    }

    private void finalizePlay() {
        int playerPoints = player.sumHandValue(false);
        int computerPoints = computer.sumHandValue(false);

        if (playerPoints > 21) {
            printComputerWin();
            player.addPoints(Points.LOSE.getValue());
            player.addLose(playerPoints - player.getVisibleCards().get(player.getVisibleCards().size()-1).getCardValue());
            return;
        }

        if (computerPoints > 21 || playerPoints == 21) {
            printPlayerWin();
            player.addPoints(Points.WIN.getValue());
            player.addWin(21 - player.getVisibleCards().get(player.getVisibleCards().size()-1).getCardValue());
            return;
        }

        if (playerPoints > computerPoints) {
            printPlayerWin();
            player.addPoints(Points.WIN.getValue());
            player.addWin(playerPoints);
        } else if (playerPoints < computerPoints) {
            printComputerWin();
            player.addPoints(Points.LOSE.getValue());
            player.addLose(playerPoints - player.getVisibleCards().get(player.getVisibleCards().size()-1).getCardValue());
        } else {
            frame.addLogLine("REMIS! Partia nierozstrzygnięta");
            player.addPoints(Points.DRAW.getValue());
        }
    }

    public Hand getCurrentHand() {
        return currentHand;
    }

    //public

    private void printComputerWin() {
        frame.addLogLine("Zwycięstwo KOMPUTERA!");
        frame.addLogLine("KOMPUTER Punkty: " + computer.sumHandValue(false) + " | Ręka: " + computer.toCompleteString());
        frame.addLogLine("GRACZ Punkty: " + player.sumHandValue(false) + " | Ręka: " + player.toCompleteString());
    }

    private void printComputerState() {
        frame.addLogLine("KOMPUTER Punkty: " + computer.sumHandValue(true) + " | Ręka: " + computer.toString());
    }

    private void printPlayerWin() {
        frame.addLogLine("Zwycięstwo GRACZA!");
        frame.addLogLine("GRACZ Punkty: " + player.sumHandValue(false) + " | Ręka: " + player.toCompleteString());
        frame.addLogLine("KOMPUTER Punkty: " + computer.sumHandValue(false) + " | Ręka: " + computer.toCompleteString());
    }

    private void printPlayerState() {
        frame.addLogLine("GRACZ Punkty: " + player.sumHandValue(false) + " | Ręka: " + player.toString());
    }

    private void printGameStart() {
        frame.addLogLine(">Nowa rozgrywka");
    }

    private void printPlayStart(int iteration) {
        frame.addLogLine("\nNowa rozgrywka nr " + iteration);
    }

    private void printComputerAction(String res) {
        frame.addLogLine("Wynik akcji komputera: " + res);
    }

    private void printPlayerAction(String res) {
        frame.addLogLine("Wynik akcji gracza: " + res);
    }

    public void printResults(int iter) {
        frame.addLogLine("Po " + iter + "rozgrywkach GRACZ uzyskał " + player.getPoints() + "punktów! \n");
        StringBuilder sb = new StringBuilder();
        sb.append("Rozkład zwycięstw: ");
        for (int i = 0; i< 22; ++i) {
            sb.append(player.getWins()[i]).append(' ');
        }
        sb.append("\nRozkład porażek: ");
        for (int i = 0; i< 22; ++i) {
            sb.append(player.getLoses()[i]).append(' ');
        }
        frame.addLogLine(sb.toString());
    }

    public void setPlayerStaretegy(Strategy strategy) {
        player.setStrategy(strategy);
    }

    public Hand getPlayer() {
        return player;
    }

    public ComputerHand getComputer() {
        return computer;
    }
}
