package bm;

import java.util.Collections;
import java.util.LinkedList;

/**
 * Pojedyncza zwykła talia kart.
 */
public class DefaultDeckCards extends DeckCards {
    int iterator;

    public DefaultDeckCards(Integer seed) {
        super(seed);
        iterator = 0;
    }

    protected void init() {
        for (CardColor color : CardColor.values()) {
            for (CardValue value : CardValue.values()) {
                getDeck().add(new Card(color, value));
            }
        }
        Collections.shuffle(getDeck(), getRandom());
    }

    public Card getNextCard() {
        ++iterator;
        if (iterator > 50) {
            getDeck().clear();
            init();
            iterator = 1;
        }
        return getDeck().get(iterator - 1);
    }

    public boolean isNextCard() {
        return getDeck().size() > iterator;
    }
}
