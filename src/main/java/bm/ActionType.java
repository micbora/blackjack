package bm;

/**
 * Typ "wyliczonej" akcji.
 */
public enum ActionType {
    INIT(false),
    NEXT_CARD(false),
    PASS(true);

    private boolean stopStatus;

    ActionType(boolean stopStatus) {
        this.stopStatus = stopStatus;
    }

    public boolean isStopStatus() {
        return stopStatus;
    }
}
