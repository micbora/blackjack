package bm;

/**
 * Karta.
 */
public class Card {

    private CardColor color;
    private CardValue value;

    public Card() {
        //empty
    }

    public Card(CardColor color, CardValue value) {
        this.color = color;
        this.value = value;
    }

    public CardColor getColor() {
        return color;
    }

    public void setColor(CardColor color) {
        this.color = color;
    }

    public CardValue getValue() {
        return value;
    }

    public void setValue(CardValue value) {
        this.value = value;
    }

    public int getCardValue() {
        return getValue().getPointValue();
    }

    @Override
    public String toString() {
        return value.toString() + color.toString();
    }
}
