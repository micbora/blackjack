package bm;

/**
 * Wartość karty.
 */
public enum CardValue {
    v2("2", 2),
    v3("3", 3),
    v4("4", 4),
    v5("5", 5),
    v6("6", 6),
    v7("7", 7),
    v8("8", 8),
    v9("9", 9),
    v10("10", 10),
    vJ("J", 10),
    vQ("Q", 10),
    vK("K", 10),
    vA("A", 11);

    private String name;
    private int pointValue;

    CardValue(String name, int pointValue) {
        this.name = name;
        this.pointValue = pointValue;
    }

    public String getName() {
        return name;
    }

    public int getPointValue() {
        return pointValue;
    }

    @Override
    public String toString() {
        return getName();
    }
}
