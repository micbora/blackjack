package bm;

/**
 * Rezultat przejścia iteracji algorytmu.
 */
public class Action {

    private String res;
    private ActionType type;

    public Action() {
    }

    public Action(String res, ActionType type) {
        this.res = res;
        this.type = type;
    }

    public String getRes() {
        return res;
    }

    public void setRes(String res) {
        this.res = res;
    }

    public ActionType getType() {
        return type;
    }

    public void setType(ActionType type) {
        this.type = type;
    }
}
