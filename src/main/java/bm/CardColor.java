package bm;

/**
 * Kolor karty.
 */
public enum CardColor {
    DIAMOND("♦"),
    HEART("♥"),
    CLOVE("♣"),
    SPADE("♠");

    String color;

    CardColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return getColor();
    }
}
