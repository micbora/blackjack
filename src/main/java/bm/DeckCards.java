package bm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Talia kart.
 */
public abstract class DeckCards {

    private List<Card> deck;
    private Random random;

    /**
     * Konstruktor.
     *
     * @param seed ziarno do zmiennej losowej
     */
    public DeckCards(Integer seed) {
        deck = new ArrayList<>();
        if (seed == -1) {
            random = new Random();
        } else {
            random = new Random(seed);
        }
        init();
        Collections.shuffle(deck, random);
    }

    /**
     * Inicjalizacja talii.
     */
    protected abstract void init();

    /**
     * Kolejna karta
     *
     * @return kolejna karta ze stosu
     */
    public abstract Card getNextCard();

    /**
     * Czy w stosie znajduje się kolejna karta?
     *
     * @return {@code true}, jeśli możliwe jest pobranie kolejnej karty ze stosu, {@code false} w przeciwnym przypadku
     */
    public abstract boolean isNextCard();

    protected List<Card> getDeck() {
        return deck;
    }

    protected Random getRandom() {
        return random;
    }
}
