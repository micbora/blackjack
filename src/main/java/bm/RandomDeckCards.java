package bm;

/**
 * Losowa, nieskończona talia kart.
 */
public class RandomDeckCards extends DeckCards {
    public RandomDeckCards(Integer seed) {
        super(seed);
    }

    protected void init() {
        //empty
    }

    public Card getNextCard() {
        CardColor color = CardColor.values()[getRandom().nextInt(CardColor.values().length)];
        CardValue value = CardValue.values()[getRandom().nextInt(CardValue.values().length)];
        return new Card(color, value);
    }

    public boolean isNextCard() {
        return true;
    }
}
