package bm;

public enum Points {
    WIN(1),
    LOSE(-1),
    CLOSE_TO_21(1),
    RANDOM(0),
    DRAW(0)
    ;

    private int value;

    Points(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
