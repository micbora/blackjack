package bm;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public enum Strategy {
    COMPUTER("Strategia krupiera", game -> {
        int val = game.getCurrentHand().sumHandValue(false);
        return  val < 17 ? new Action(game.getCurrentHand().getName() + " kontynuuje grę", ActionType.NEXT_CARD) : new Action(game.getCurrentHand().getName() + " kończy grę", ActionType.PASS);
    }),
    EXTREME("Strategia ekstremalna z wykładu", game -> {
        int val = game.getCurrentHand().sumHandValue(false);
        return  val < 20 ? new Action(game.getCurrentHand().getName() + " kontynuuje grę", ActionType.NEXT_CARD) : new Action(game.getCurrentHand().getName() + " kończy grę", ActionType.PASS);
    }),
    NEVER_BUST("Never bust",  game -> {
        int val = game.getCurrentHand().sumHandValue(false);
        return  val < 12 ? new Action(game.getCurrentHand().getName() + " kontynuuje grę", ActionType.NEXT_CARD) : new Action(game.getCurrentHand().getName() + " kończy grę", ActionType.PASS);
    }),
    BASE("Strategia podstawowa", new Function<Game, Action>() {
        @Override
        public Action apply(Game game) {
            List<Card> cards = game.getPlayer().getVisibleCards();
            Map<CardValue, Integer> map = new ConcurrentHashMap<>();
            for (CardValue cv : CardValue.values()) {
                map.put(cv, 0);
            }
            for (Card c : cards) {
                Integer i = map.get(c.getValue());
                map.put(c.getValue(), ++i);
            }
            for (CardValue cv : CardValue.values()) {
                int i = map.get(cv);
                if (i >= 2) {
                    switch (cv) {
                        case vA:
                        case v2:
                        case v3:
                        case v4:
                        case v5:
                            return new Action(game.getCurrentHand().getName() + " kontynuuje grę", ActionType.NEXT_CARD);
                        case v6:
                            return  game.getComputer().sumHandValue(true) >= 7 ? new Action(game.getCurrentHand().getName() + " kontynuuje grę", ActionType.NEXT_CARD) : new Action(game.getCurrentHand().getName() + " kończy grę", ActionType.PASS);
                        case v7:
                            return  game.getComputer().sumHandValue(true) >= 8 ? new Action(game.getCurrentHand().getName() + " kontynuuje grę", ActionType.NEXT_CARD) : new Action(game.getCurrentHand().getName() + " kończy grę", ActionType.PASS);
                        case v8:
                            return  game.getComputer().sumHandValue(true) >= 10 ? new Action(game.getCurrentHand().getName() + " kontynuuje grę", ActionType.NEXT_CARD) : new Action(game.getCurrentHand().getName() + " kończy grę", ActionType.PASS);
                        case v9:
                        case vJ:
                        case vK:
                        case vQ:
                        case v10:
                            return new Action(game.getCurrentHand().getName() + " kończy grę", ActionType.PASS);
                    }
                }
            }
            if(map.get(CardValue.vA) == 1) {
                List<CardValue> values = new ArrayList<>();
                values.addAll(Arrays.asList(CardValue.values()));
                List<CardValue> values1 = new LinkedList<>();
                for(int i = values.size()-1; i>=0; --i) {
                    values1.add(values.get(i));
                }
                for (CardValue cv : values1) {
                    if(!cv.equals(CardValue.vA) && map.get(cv) > 0) {
                        switch (cv) {
                            case vA:
                            case v2:
                            case v3:
                            case v4:
                            case v5:
                            case v6:
                                return new Action(game.getCurrentHand().getName() + " kontynuuje grę", ActionType.NEXT_CARD);
                            case v7:
                                return  game.getComputer().sumHandValue(true) >= 9 ? new Action(game.getCurrentHand().getName() + " kontynuuje grę", ActionType.NEXT_CARD) : new Action(game.getCurrentHand().getName() + " kończy grę", ActionType.PASS);
                            case v8:
                            case v9:
                                return new Action(game.getCurrentHand().getName() + " kończy grę", ActionType.PASS);
                            default:
                                return new Action(game.getCurrentHand().getName() + " kończy grę", ActionType.PASS);
                        }
                    }
                }
            }
            int playerPoints = game.getPlayer().sumHandValue(false);
            if(playerPoints < 11) {
                return new Action(game.getCurrentHand().getName() + " kontynuuje grę", ActionType.NEXT_CARD);
            } else if (playerPoints == 12) {
                if (game.getComputer().getVisibleCards().get(0).getCardValue() >= 4 && game.getComputer().getVisibleCards().get(0).getCardValue() <= 6) {
                    return new Action(game.getCurrentHand().getName() + " kończy grę", ActionType.PASS);
                } else {
                    return new Action(game.getCurrentHand().getName() + " kontynuuje grę", ActionType.NEXT_CARD);
                }
            } else if (playerPoints <=16) {
                if (game.getComputer().getVisibleCards().get(0).getCardValue() > 6) {
                    return new Action(game.getCurrentHand().getName() + " kontynuuje grę", ActionType.NEXT_CARD);
                } else {
                    return new Action(game.getCurrentHand().getName() + " kończy grę", ActionType.PASS);
                }
            } else {
                return new Action(game.getCurrentHand().getName() + " kończy grę", ActionType.PASS);
            }
        }
    })
    ;

    private String name;
    private Function<Game, Action> function;

    Strategy(String name, Function<Game, Action> function) {
        this.name = name;
        this.function = function;
    }

    public String getName() {
        return name;
    }

    public Function<Game, Action> getFunction() {
        return function;
    }

    @Override
    public String toString() {
        return getName();
    }
}
